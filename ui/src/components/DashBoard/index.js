export const columns = [
    {
        title: '服务模块',
        dataIndex: 'module',
        width:150,
        fixed:true,
        filters: [
            { text: '用户模块', value: '用户模块' },
            { text: '鉴权模块', value: '鉴权模块' },
        ],
    },
    {
        title: '状态',
        dataIndex: 'status',
        width:150,
        fixed:true,
        filters: [
            { text: '成功', value: 1 },
            { text: '失败', value: 0 },
        ],
    },
    {
        title: '接口方法',
        dataIndex: 'method',
        width:180,
        fixed:true,
        filters: [
            { text: '新增', value: 'CREATE' },
            { text: '删除', value: 'DELETE' },
            { text: '修改', value: 'UPDATE' },
            { text: '查询', value: 'READ' }
        ],
    },
    {
        title: '请求来源',
        dataIndex: 'reqSource',
        customFilterDropdown: true,
        width:220
    },
    {
        title: '请求时间',
        sorter: true,
        width:160,
        dataIndex: 'startTime',
    },

    {
        title: '请求参数',
        dataIndex: 'params',
    },
    {
        title: '响应数据',
        dataIndex: 'returnData',
    },
    
    {
        title: '控制器',
        dataIndex: 'clazz',
    }
   
]
export const defaultQueryParams = {
    pageSize: 10,
    pageNum: 1,
    sortBy:'startTime',
    order:'DESC',
    modules:[],
    actions:[],
    statuses:[],
    operatorId:undefined
}