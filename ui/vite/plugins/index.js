import vue from '@vitejs/plugin-vue'
import createAutoImport from './auto-import'
import createComponentsImport from './component-import'
import createSetupExtend from './setup-extend'
export default function createVitePlugins(viteEnv, isBuild = false) {
    const vitePlugins = [vue()]
    vitePlugins.push(createAutoImport())
    vitePlugins.push(createComponentsImport())
    vitePlugins.push(createSetupExtend())
    return vitePlugins
}
