import autoImport from 'unplugin-auto-import/vite'
import {
    ElementPlusResolver,
    AntDesignVueResolver,
    VantResolver,
    NaiveUiResolver
} from 'unplugin-vue-components/resolvers'
export default function createAutoImport() {
    return autoImport({
        resolvers: [ElementPlusResolver(),AntDesignVueResolver(),VantResolver(),NaiveUiResolver()],
        imports: [
            'vue',
            '@vueuse/core'
        ],
        dts: false
    })
}
