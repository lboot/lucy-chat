#### 插件功能

**unplugin-auto-import**

为 Vite、Webpack、Rollup 和 esbuild 按需自动导入`API`



**unplugin-vue-components**

Vue 的**按需组件自动导入**

https://juejin.cn/post/7012446423367024676

**vite-plugin-icons**

基于`Iconify`图标的vite插件支持,让你方便的使用iconify图标(真的超方便),可以配合`vcode`的`Iconify IntelliSense`插件使用.

https://juejin.cn/post/6998059092497399845#heading-7

```vue
<script setup>
import IconAccessibility from 'virtual:vite-icons/carbon/accessibility'
import IconAccountBox from 'virtual:vite-icons/mdi/account-box'
</script>

<template>
  <icon-accessibility/>
  <icon-account-box style="font-size: 2em; color: red"/>
</template>
```



**vite-plugin-vue-setup-extend**

借助插件vite-plugin-vue-setup-extend可以让我们更优雅的解决这个问题，可以直接在script标签上定义name。实现`keep-alive`



**vite-plugin-pages**

为 `pages` 目录中的 Vue 组件自动生成路由配置，同时支持布局的 Vite 插件。

- 以 `_` 开头命名的组件为动态页面
- 以 `$` 开头命名的组件为嵌套路由
- 文件名为 `_` 的组件为错误捕获页面
- 默认布局将被应用至 `pages` 目录下的所有 Vue 组件
- 自定义布局只生效于在 `layout` 块语句中定义相应了布局的 Vue 组件中

参考以下示例：

1. 通用的文件名：`/user/foo.vue` -> `/user/foo`
2. 以 `_` 开头命名的文件：`/user/_id.vue` -> `/user/:id`
3. 文件名为 `index.vue`：`/user/index.vue` -> `/user`
4. 文件名为 `_`：`/_.vue` -> `/:pathMatch(.*)*`



```js
Pages({
  pagesDir: [
   { dir: 'src/pages', baseRoute: '' },
   // src/features/pages文件夹下会生成/features/filename这样的路由
   { dir: 'src/features/pages', baseRoute: 'features' },
   // 会识别fruits下多个分类下pages的文件
   { dir: 'src/fruits/**/pages', baseRoute: 'fruits' },
  ],
})
```



https://juejin.cn/post/7016585379235135496

https://blog.csdn.net/qq_42611074/article/details/123026428

https://www.cnblogs.com/guozhiqiang/p/16892086.html

https://originjs.org/guide/plugins/vite-plugin-pages/

https://www.jiayupearl.shop/2022/05/31/Vite-Plugin-Pages%E4%B8%8EKeep-Alive/

https://juejin.cn/post/7069595464215035918
