import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import createVitePlugins from './vite/plugins'
import { resolve } from 'path'
// https://vitejs.dev/config/
export default defineConfig({
  plugins: createVitePlugins(),
  // vite 相关配置
  server: {
    port: 81,
    host: true,
    open: true,
    resolve: {
      // https://cn.vitejs.dev/config/#resolve-alias
      alias: {
        // 设置路径
        '~': resolve(__dirname, './'),
        // 设置别名
        '@': resolve(__dirname, './src')
      },
      // https://cn.vitejs.dev/config/#resolve-extensions
      extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json', '.vue']
    },
    proxy: {
      // https://cn.vitejs.dev/config/#server-proxy
      '/dev-api': {
        target: 'http://localhost:8080',
        //target: 'http://pms.apisev.cn',
        changeOrigin: true,
        rewrite: (p) => p.replace(/^\/dev-api/, '')
      },
      '/proxy-api': {
        target: 'http://localhost:8080',
        //target: 'http://pms.apisev.cn',
        changeOrigin: true,
        rewrite: (p) => p.replace(/^\/proxy-api/, '')
      }
    },
    build: {
      rollupOptions: {
        input: {
          main: resolve(__dirname, 'index.html'),
          dashboard: resolve(__dirname, 'dashboard.html'),
        }
      }
    }
  }
})
