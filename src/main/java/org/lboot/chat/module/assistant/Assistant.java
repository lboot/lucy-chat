package org.lboot.chat.module.assistant;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.lboot.chat.module.common.Tool;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * "id": "asst_abc123",
 *   "object": "assistant",
 *   "created_at": 1698984975,
 *   "name": "Math Tutor",
 *   "description": null,
 *   "model": "gpt-4",
 *   "instructions": "You are a personal math tutor. When asked a question, write and run Python code to answer the question.",
 *   "tools": [
 *     {
 *       "type": "code_interpreter"
 *     }
 *   ],
 *   "file_ids": [],
 *   "metadata": {}
 */
@Data
@Slf4j
@ApiModel(value = "助手实体")
public class Assistant implements Serializable {
    @ApiModelProperty(value = "助手ID")
    private String id;

    @ApiModelProperty(value = "使用模型",example = "gpt-4")
    private String model;
    /**
     * 助理的名字。最大长度为 256 个字符。
     */
    @ApiModelProperty(value = "助手名称")
    private String name;
    /**
     * 助理的描述。最大长度为 512 个字符。
     */
    @ApiModelProperty(value = "助手描述")
    private String description;
    /**
     * 助手使用的系统指令。最大长度为 32768 个字符。
     */
    @ApiModelProperty(value = "助手系统指令")
    private String instructions;
    /**
     * 助手上启用的工具列表。每个助手最多可以有 128 个工具。工具可以是 code_interpreter、retrieval或function。
     */
    @ApiModelProperty(value = "工具列表")
    private List<Tool> tools;
    /**
     * 附加到该助手的文件 ID 列表。助手最多可以附加 20 个文件。文件按其创建日期升序排列。
     */
    @ApiModelProperty(value = "助手文件列表")
    @JsonProperty("file_ids")
    private List<String> fileIds;
    /**
     * Set of 16 key-value pairs that can be attached to an object.
     * This can be useful for storing additional information about the object in a structured format.
     * Keys can be a maximum of 64 characters long and values can be a maxium of 512 characters long.
     */
    @ApiModelProperty(value = "元数据")
    private Map<String,Object> metadata;


    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss", timezone = "GMT+8")
    @JsonProperty("created_at")
    private Long createdAt;
}
