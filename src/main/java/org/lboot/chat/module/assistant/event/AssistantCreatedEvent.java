package org.lboot.chat.module.assistant.event;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.lboot.chat.module.assistant.Assistant;
import org.springframework.context.ApplicationEvent;

import java.time.Clock;

/**
 * @author kindear
 * 助手已创建事件
 */
@Slf4j
@Getter
public class AssistantCreatedEvent extends ApplicationEvent {
    Assistant assistant;
    public AssistantCreatedEvent(Object source, Assistant assistant) {
        super(source);
        this.assistant = assistant;
    }

    public AssistantCreatedEvent(Object source, Clock clock) {
        super(source, clock);
    }
}
