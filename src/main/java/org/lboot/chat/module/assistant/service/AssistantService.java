package org.lboot.chat.module.assistant.service;

import org.lboot.chat.module.assistant.Assistant;
import org.lboot.chat.module.assistant.params.AssistantParams;
import org.lboot.chat.module.assistant.params.AssistantQueryParams;
import org.lboot.chat.module.common.DeleteResult;
import org.lboot.chat.module.common.ListResult;
import org.lboot.chat.module.file.model.ChatFile;

/**
 * @author kindear
 * 助理相关服务
 */
public interface AssistantService {
    Assistant createAssistant(AssistantParams params);

    DeleteResult deleteAssistant(String assistantId);


    ListResult<Assistant> listAssistant(AssistantQueryParams params);

    ChatFile attachAssistantFile(String assistantId, String fileId);
}
