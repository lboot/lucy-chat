package org.lboot.chat.module.assistant.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.lboot.chat.api.assistant.AssistantApi;
import org.lboot.chat.api.assistant.AssistantFileApi;
import org.lboot.chat.module.assistant.Assistant;
import org.lboot.chat.module.assistant.event.AssistantCreatedEvent;
import org.lboot.chat.module.assistant.params.AssistantParams;
import org.lboot.chat.module.assistant.params.AssistantQueryParams;
import org.lboot.chat.module.common.DeleteResult;
import org.lboot.chat.module.common.ListResult;
import org.lboot.chat.module.file.model.ChatFile;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Slf4j
@Service
@AllArgsConstructor
public class AssistantServiceImpl implements AssistantService {
    AssistantApi assistantApi;

    AssistantFileApi assistantFileApi;

    @Resource
    ApplicationContext context;
    @Override
    public Assistant createAssistant(AssistantParams params) {
        Assistant assistant = assistantApi.createAssistant(params);
        context.publishEvent(new AssistantCreatedEvent(this, assistant));
        return assistant;
    }

    @Override
    public DeleteResult deleteAssistant(String assistantId) {
        return assistantApi.deleteAssistant(assistantId);
    }

    @Override
    public ListResult<Assistant> listAssistant(AssistantQueryParams params) {
        return assistantApi.listAssistant(params);
    }

    @Override
    public ChatFile attachAssistantFile(String assistantId, String fileId) {
        return assistantFileApi.attachAssistantFile(assistantId, fileId);
    }
}
