package org.lboot.chat.module.assistant.params;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.lboot.chat.module.common.Tool;

import java.util.List;

@Data
@Slf4j
@ApiModel(value = "助手新建/更新参数")
public class AssistantParams {
    /**
     * 助手使用的系统指令。最大长度为 32768 个字符。
     */
    @ApiModelProperty(value = "助手使用的系统指令",example = "你是一个计算机方面的专家，擅长数据挖掘等技能工具")
    private String instructions;

    @ApiModelProperty(value = "助手介绍",example = "计算机专家助理")
    private String description;
    /**
     * 助手名称
     */
    @ApiModelProperty(value = "助手名称",example = "计算机专家")
    private String name;
    /**
     * 助手上启用的工具列表。每个助手最多可以有 128 个工具。工具可以是 code_interpreter、retrieval或function。
     */
    @ApiModelProperty(value = "助手上启用的工具列表")
    private List<Tool> tools;
    /**
     * 助手模型
     */
    @ApiModelProperty(value = "模型",example = "gpt-4")
    private String model;
}
