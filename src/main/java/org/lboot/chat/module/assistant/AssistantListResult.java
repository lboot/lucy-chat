package org.lboot.chat.module.assistant;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @author kindear
 * 助理列表结果
 */
@Deprecated
@Slf4j
@Data
@ApiModel(value = "助手列表")
public class AssistantListResult {
    @ApiModelProperty(value = "助手信息列表")
    List<Assistant> data;
    @ApiModelProperty(value = "返回结果类型")
    private String object;
    @ApiModelProperty(value = "起始助手ID")
    private String first_id;
    @ApiModelProperty(value = "结束助手ID")
    private String last_id;
    @ApiModelProperty(value = "更多")
    private Boolean has_more;

}
