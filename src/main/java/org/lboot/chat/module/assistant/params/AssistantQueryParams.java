package org.lboot.chat.module.assistant.params;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @author kindear
 * 助手查询参数
 */
@Slf4j
@Data
@ApiModel(value = "助理查询参数")
public class AssistantQueryParams {
    @ApiModelProperty(value = "数量限制")
    private Integer limit;

    @ApiModelProperty(value = "排序条件",notes = "aes/desc")
    String order;

    @ApiModelProperty(value = "助理ID(之后)")
    String after;

    @ApiModelProperty(value = "助理ID(之前)")
    String before;

}
