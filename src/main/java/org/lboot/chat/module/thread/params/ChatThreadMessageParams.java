package org.lboot.chat.module.thread.params;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.lboot.chat.module.common.message.ThreadMessage;

/**
 * @author kindear
 * 聊天信息参数
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "Thread聊天信息创建")
public class ChatThreadMessageParams extends ThreadMessage {

}
