package org.lboot.chat.module.thread.message;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import org.lboot.chat.module.thread.message.content.Content;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Data
@ApiModel(value = "Thread信息")
public class ChatThreadMessage {
    private String id;

    private String object;
    /**
     * 创建时间戳
     */
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss", timezone = "GMT+8")
    @JsonProperty("created_at")
    private Date createdAt;

    @JsonProperty("thread_id")
    private String threadId;

    private String role;
    /**
     * 文本或图像数组中的消息内容
     */
    private List<Content> content;

    @JsonProperty("assistant_id")
    private String assistantId;

    @JsonProperty("run_id")
    private String runId;

    /**
     * 附加到该助手的文件 ID 列表。助手最多可以附加 20 个文件。文件按其创建日期升序排列。
     */
    @JsonProperty("file_ids")
    private List<String> fileIds;

    /**
     * Set of 16 key-value pairs that can be attached to an object.
     * This can be useful for storing additional information about the object in a structured format.
     * Keys can be a maximum of 64 characters long and values can be a maxium of 512 characters long.
     */
    private Map<String, Object> metadata;
}
