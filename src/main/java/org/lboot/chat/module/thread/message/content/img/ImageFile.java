package org.lboot.chat.module.thread.message.content.img;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ImageFile {

    @JsonProperty("file_id")
    private String fileId;
}
