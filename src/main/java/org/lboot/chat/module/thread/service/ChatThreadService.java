package org.lboot.chat.module.thread.service;

import org.lboot.chat.module.common.CreateResult;
import org.lboot.chat.module.common.DeleteResult;
import org.lboot.chat.module.common.ListResult;
import org.lboot.chat.module.thread.message.ChatThreadMessage;
import org.lboot.chat.module.thread.params.ChatThreadParams;
import org.lboot.chat.module.thread.params.ChatThreadRunParams;
import org.lboot.chat.module.thread.run.ChatThreadRun;

import java.util.Map;

public interface ChatThreadService {
    CreateResult createThread(ChatThreadParams params);

    DeleteResult deleteThread(String threadId);

    CreateResult updateThread(String threadId, Map<String,Object> metadata);


    CreateResult getThread(String threadId);

    ListResult<ChatThreadMessage> getThreadMessages(String threadId);

    ChatThreadRun threadRun(String threadId, ChatThreadRunParams params);
}
