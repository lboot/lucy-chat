package org.lboot.chat.module.thread.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.lboot.chat.api.thread.MessageApi;
import org.lboot.chat.api.thread.RunApi;
import org.lboot.chat.api.thread.ThreadApi;
import org.lboot.chat.module.common.CreateResult;
import org.lboot.chat.module.common.DeleteResult;
import org.lboot.chat.module.common.ListResult;
import org.lboot.chat.module.thread.message.ChatThreadMessage;
import org.lboot.chat.module.thread.params.ChatThreadParams;
import org.lboot.chat.module.thread.params.ChatThreadRunParams;
import org.lboot.chat.module.thread.run.ChatThreadRun;
import org.springframework.stereotype.Service;

import java.util.Map;

@Slf4j
@Service
@AllArgsConstructor
public class ChatThreadServiceImpl implements ChatThreadService{
    ThreadApi threadApi;

    MessageApi messageApi;

    RunApi runApi;

    @Override
    public CreateResult createThread(ChatThreadParams params) {
        return threadApi.createThread(params);
    }

    @Override
    public DeleteResult deleteThread(String threadId) {
        return threadApi.deleteThread(threadId);
    }

    @Override
    public CreateResult updateThread(String threadId, Map<String, Object> metadata) {
        return threadApi.updateThread(threadId, metadata);
    }

    @Override
    public CreateResult getThread(String threadId) {
        return threadApi.getThread(threadId);
    }

    @Override
    public ListResult<ChatThreadMessage> getThreadMessages(String threadId) {
        return messageApi.listThreadMessage(threadId);
    }

    @Override
    public ChatThreadRun threadRun(String threadId, ChatThreadRunParams params) {
        return runApi.createThreadRun(threadId, params);
    }
}
