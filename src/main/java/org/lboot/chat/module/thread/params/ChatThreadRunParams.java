package org.lboot.chat.module.thread.params;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.lboot.chat.module.common.Tool;

import java.util.List;
import java.util.Map;

@Data
@ApiModel(value = "会话执行参数")
public class ChatThreadRunParams {
    @ApiModelProperty(value = "助手ID")
    private String assistant_id;

    /**
     * 助手模型
     */
    @ApiModelProperty(value = "模型",example = "gpt-4")
    private String model;
    /**
     * 助手使用的系统指令。最大长度为 32768 个字符。
     */
    @ApiModelProperty(value = "助手使用的系统指令",example = "你是一个计算机方面的专家，擅长数据挖掘等技能工具")
    private String instructions;

    /**
     * 助手上启用的工具列表。每个助手最多可以有 128 个工具。工具可以是 code_interpreter、retrieval或function。
     */
    @ApiModelProperty(value = "助手上启用的工具列表")
    private List<Tool> tools;

    @ApiModelProperty(value = "元数据")
    private Map<String,Object> metadata;
}
