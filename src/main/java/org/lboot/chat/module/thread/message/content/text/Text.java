package org.lboot.chat.module.thread.message.content.text;

import lombok.Data;

import java.util.List;

@Data
public class Text {
    private String value;

    List<String> annotations;
}
