package org.lboot.chat.module.thread.message.content;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import org.lboot.chat.module.thread.message.content.img.ImageFile;
import org.lboot.chat.module.thread.message.content.text.Text;

@Data
@ApiModel(value = "消息内容")
public class Content {
    /**
     * 输入类型：text、image_file
     *
     * @see Type
     */
    private String type;

    private Text text;

    @JsonProperty("image_file")
    private ImageFile imageFile;

    /**
     * 类型
     */
    @Getter
    @AllArgsConstructor
    public enum Type {
        TEXT("text"),
        IMAGE_FILE("image_file"),
        ;
        private final String name;
    }

}
