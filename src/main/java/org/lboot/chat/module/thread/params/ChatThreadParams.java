package org.lboot.chat.module.thread.params;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.lboot.chat.module.common.message.ThreadMessage;

import java.util.List;
import java.util.Map;

@Data
@ApiModel(value = "Thread新建参数")
public class ChatThreadParams {
    @ApiModelProperty(value = "消息列表")
    List<ThreadMessage> messages;

    @ApiModelProperty(value = "元数据")
    Map<String ,Object> metadata;
}
