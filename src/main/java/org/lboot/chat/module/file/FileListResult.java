package org.lboot.chat.module.file;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.lboot.chat.module.file.model.ChatFile;

import java.util.List;

/**
 * @author kindear
 * 文件列表结果
 */
@Data
@ApiModel(value = "文件列表")
public class FileListResult {
    @ApiModelProperty(value = "文件信息列表")
    List<ChatFile> data;
    @ApiModelProperty(value = "返回结果类型")
    private String object;
    @ApiModelProperty(value = "更多")
    private Boolean has_more;
}
