package org.lboot.chat.module.file.params;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
@ApiModel(value = "网络文件上传参数")
public class WebFileParams {
    @ApiModelProperty(value = "上传文件")
    MultipartFile file;

    @ApiModelProperty(value = "文件目的",example = "assistants",notes = "assistants 助手文件 fine-tune 微调文件")
    private String purpose;
}
