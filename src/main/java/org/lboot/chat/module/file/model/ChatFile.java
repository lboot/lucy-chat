package org.lboot.chat.module.file.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 *  {
 *       "id": "file-abc123",
 *       "object": "file",
 *       "bytes": 175,
 *       "created_at": 1613677385,
 *       "filename": "salesOverview.pdf",
 *       "purpose": "assistants",
 *     },
 */
@Data
@ApiModel(value = "ChatGPT文件实体")
public class ChatFile {
    @ApiModelProperty(value = "文件ID")
    private String id;
    @ApiModelProperty(value = "类型")
    private String object;
    @ApiModelProperty(value = "文件大小")
    private Integer bytes;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss", timezone = "GMT+8")
    @JsonProperty("created_at")
    private Date createdAt;

    @ApiModelProperty(value = "文件名称")
    private String filename;

    @ApiModelProperty(value = "文件目的")
    private String purpose;
}
