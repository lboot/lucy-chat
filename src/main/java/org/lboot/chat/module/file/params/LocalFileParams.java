package org.lboot.chat.module.file.params;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.File;

/**
 * @author kindear
 */
@Data
@ApiModel(value = "本地文件上传参数")
public class LocalFileParams {
    @ApiModelProperty(value = "上传文件")
    File file;

    @ApiModelProperty(value = "文件目的",example = "assistants",notes = "assistants 助手文件 fine-tune 微调文件")
    private String purpose;
}
