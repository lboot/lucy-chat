package org.lboot.chat.module.chat.service;

import org.lboot.chat.module.chat.ChatParams;
import org.lboot.chat.module.chat.ChatResult;
import org.lboot.chat.module.common.message.ChatMessage;
import org.lboot.chat.module.edit.EditParams;
import org.lboot.chat.module.edit.EditResult;
import org.lboot.chat.module.image.ImageParams;
import org.lboot.chat.module.image.ImageResult;

import java.util.List;
import java.util.Map;

/**
 * 使用ChatGpt 相关服务接口
 */
public interface ChatService {
    // 构建请求头
    Map<String,Object> headers();

    // 构建用户消息
    ChatMessage buildUserMessage(String content);

    // 构建会话
    ChatResult doChat(ChatParams params);

    // 构建会话 V2 -- 携带会话ID
    ChatResult doChat(ChatParams params, String chatId);


    // 构建编辑执行
    EditResult doEdit(EditParams params);

    // 构建图形绘制
    ImageResult doDraw(ImageParams params);

    // 获取上下文
    List<ChatMessage> getContext(String chatId);

    // 获取上下文 -- 指定条数
    List<ChatMessage> getContext(String chatId, Integer num);

    // 简化返回结果
    String simpleResult(ChatResult result);

    // 简化编辑返回结果
    String simpleResult(EditResult result);

    // 简化图片绘制返回结果
    List<String> simpleResult(ImageResult result);
}
