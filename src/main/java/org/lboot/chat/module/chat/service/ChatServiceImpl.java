package org.lboot.chat.module.chat.service;

import org.lboot.chat.api.chat.ChatApi;
import org.lboot.chat.api.edit.EditApi;
import org.lboot.chat.api.image.ImageApi;
import org.lboot.chat.cache.ChatCache;
import org.lboot.chat.constant.ChatRoleConst;
import org.lboot.chat.module.chat.ChatParams;
import org.lboot.chat.module.chat.ChatResult;
import org.lboot.chat.module.chat.model.ChoiceModel;
import org.lboot.chat.module.common.message.ChatMessage;
import org.lboot.chat.module.edit.EditParams;
import org.lboot.chat.module.edit.EditResult;
import org.lboot.chat.module.edit.model.EditChoiceModel;
import org.lboot.chat.module.image.ImageParams;
import org.lboot.chat.module.image.ImageResult;
import org.lboot.chat.module.image.model.ImageChoiceModel;
import org.lboot.chat.service.ChatAuditService;
import org.lboot.chat.service.ChatCacheService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author kindear
 * OpenAI相关服务实现
 */
@Slf4j
@Service
public class ChatServiceImpl implements ChatService {
    @Value("${openai.chat.key}")
    private String chatKey;

    @Resource
    private ChatApi chatApi;

    @Resource
    private EditApi editApi;

    @Resource
    private ImageApi imageApi;

    @Autowired
    ChatAuditService auditService;

    @Autowired
    ChatCacheService cacheService;

    @Override
    public Map<String, Object> headers() {
        Map<String,Object> headers = new HashMap<>();
        headers.put("Authorization","Bearer " + chatKey);
        return headers;
    }

    @Override
    public ChatMessage buildUserMessage(String content) {
        ChatMessage message = new ChatMessage();
        message.setRole(ChatRoleConst.USER);
        message.setContent(content);
        return message;
    }

    @Override
    public ChatResult doChat(ChatParams params) {
        // 写入用户ID
        params.setUser(auditService.getUserId());
        return chatApi.ChatCompletions(params);
    }

    @Override
    public ChatResult doChat(ChatParams params, String chatId) {
        // 写入用户ID
        params.setUser(auditService.getUserId());
        ChatResult result = chatApi.ChatCompletions(params);
        // 获取返回结果
        List<ChoiceModel> choices = result.getChoices();
        // 获取第一条结果
        ChoiceModel choice = choices.get(0);
        // 获取消息
        ChatMessage message = choice.getMessage();
        // 写入缓存
        log.info(message.toString());
        // 2023-04-10 用服务方法实现替代原对缓存的直接操作
        cacheService.write(chatId,message);
        // 返回结果
        return result;
    }


    @Override

    public EditResult doEdit(EditParams params) {
        EditResult result = editApi.ChatEdits(headers(),params);
        return result;
    }

    @Override
    public ImageResult doDraw(ImageParams params) {
        // 写入用户
        params.setUser(auditService.getUserId());
        ImageResult result = imageApi.ChatDraw(headers(),params);
        return result;
    }

    @Override
    public List<ChatMessage> getContext(String chatId) {
        List<ChatMessage> messages = ChatCache.get(chatId);
        return messages;
    }


    @Override
    public List<ChatMessage> getContext(String chatId, Integer num) {
        List<ChatMessage> messages = getContext(chatId);
        int msgSize = messages.size();

        if (msgSize > num){
            return messages.subList(msgSize - num,msgSize);
        }else {
            return messages;
        }
    }

    @Override
    public String simpleResult(ChatResult result) {
        // 获取返回结果
        List<ChoiceModel> choices = result.getChoices();
        // 获取第一条结果
        ChoiceModel choice = choices.get(0);
        // 获取消息
        ChatMessage message = choice.getMessage();
        // 返回内容
        return message.getContent();
    }

    @Override
    public String simpleResult(EditResult result) {
        List<EditChoiceModel> choices = result.getChoices();
        // 获取结果
        EditChoiceModel choice = choices.get(0);
        // 获取消息
        return choice.getText();
    }

    @Override
    public List<String> simpleResult(ImageResult result) {
        List<ImageChoiceModel> choices = result.getData();
        List<String> res = choices.stream().map(ImageChoiceModel::getUrl).collect(Collectors.toList());
        return res;
    }
}
