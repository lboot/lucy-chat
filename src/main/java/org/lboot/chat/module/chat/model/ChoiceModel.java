package org.lboot.chat.module.chat.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.lboot.chat.module.common.message.ChatMessage;

@Data
@ApiModel(value = "结果集")
public class ChoiceModel {
    @ApiModelProperty(value = "结果下标")
    Integer index;

    /**
     * stream 为 false 返回结果为 delta
     */
    @ApiModelProperty(value = "可选结果")
    ChatMessage message;

    @ApiModelProperty(value = "日志概率",example = "false")
    Boolean logprobs;

    @ApiModelProperty(value = "终止原因")
    String finish_reason;

    /**
     * stream 为 true 返回结果为 delta
     */
    @ApiModelProperty(value = "结果",notes = "stream 为 true")
    ChatMessage delta;
}
