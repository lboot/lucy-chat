package org.lboot.chat.module.chat.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * "prompt_tokens": 30,
 * 		"completion_tokens": 282,
 * 		"total_tokens": 312
 */
@Data
@ApiModel(value = "TOKEN消耗统计")
public class UsageModel {
    @ApiModelProperty(value = "问题消耗")
    private Integer prompt_tokens;

    @ApiModelProperty(value = "回复消耗")
    private Integer completion_tokens;

    @ApiModelProperty(value = "总消耗")
    private Integer total_tokens;
}
