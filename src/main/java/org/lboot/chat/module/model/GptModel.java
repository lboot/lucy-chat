package org.lboot.chat.module.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;

/**
 * @author kindear
 * ChatGPT 支持模型
 * "id": "model-id-0",
 *       "object": "model",
 *       "created": 1686935002,
 *       "owned_by": "organization-owner"
 */
@Data
@Slf4j
@ApiModel(value = "GPT模型信息实体")
public class GptModel {
    @ApiModelProperty(value = "模型ID")
    private String id;
    @ApiModelProperty(value = "数据类型")
    private String object;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss", timezone = "GMT+8")
    @ApiModelProperty(value = "创建日期")
    private Date created;
    @ApiModelProperty(value = "拥有人")
    private String owned_by;
}
