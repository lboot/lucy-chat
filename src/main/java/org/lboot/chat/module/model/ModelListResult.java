package org.lboot.chat.module.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "模型列表")
public class ModelListResult {
    @ApiModelProperty(value = "模型信息列表")
    List<GptModel> data;
    @ApiModelProperty(value = "返回结果类型")
    private String object;
}
