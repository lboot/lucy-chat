package org.lboot.chat.module.common.message;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.lboot.chat.module.common.Tool;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "助手消息")
public class AssistantMessage extends ChatMessage {
    @ApiModelProperty(value = "工具调用")
    List<Tool> tool_calls;
}
