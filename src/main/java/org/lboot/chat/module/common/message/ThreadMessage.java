package org.lboot.chat.module.common.message;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "Thread消息")
public class ThreadMessage extends ChatMessage{
    @ApiModelProperty(value = "文件Id列表")
    List<String> file_ids;

    @ApiModelProperty(value = "补充数据")
    Map<String ,Object> metadata;
}
