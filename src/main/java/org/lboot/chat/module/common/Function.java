package org.lboot.chat.module.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "函数")
public class Function {
    @ApiModelProperty(value = "函数名称")
    String name;

    @ApiModelProperty(value = "函数参数")
    String arguments;
}
