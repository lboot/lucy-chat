package org.lboot.chat.module.common.message;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Deprecated
@Data
@ApiModel(value = "函数消息")
public class FunctionMessage extends ChatMessage{

}
