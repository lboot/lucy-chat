package org.lboot.chat.module.common.message;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author kindear
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "工具消息")
public class ToolMessage extends ChatMessage{
    @ApiModelProperty(value = "调用工具ID")
    String tool_call_id;
}
