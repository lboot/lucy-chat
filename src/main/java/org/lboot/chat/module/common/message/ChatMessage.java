package org.lboot.chat.module.common.message;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
//用户消息/系统消息
@ApiModel(value = "消息公共类")
public class ChatMessage {
    @ApiModelProperty(value = "聊天角色",example = "user")
    String role;

    @ApiModelProperty(value = "聊天内容",example = "hello")
    String content;

//    @ApiModelProperty(value = "用户名称",example = "牛牛/助理")
//    String name;
}
