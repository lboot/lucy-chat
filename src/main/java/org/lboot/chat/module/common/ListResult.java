package org.lboot.chat.module.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.List;


@Slf4j
@Data
@ApiModel(value = "查询结果")
public class ListResult<T> {
    @ApiModelProperty(value = "信息列表")
    List<T> data;
    @ApiModelProperty(value = "返回结果类型")
    private String object;
    @ApiModelProperty(value = "起始ID")
    private String first_id;
    @ApiModelProperty(value = "结束ID")
    private String last_id;
    @ApiModelProperty(value = "更多")
    private Boolean has_more;

}
