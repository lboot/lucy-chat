package org.lboot.chat.module.common;

import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

@Slf4j
@Data
public class Tool implements Serializable {

    private String type;

    /**
     * type为function时，function参数必输
     */
    private Function function;


    /**
     * 支持的三种类型
     */
    @Getter
    @AllArgsConstructor
    public enum Type {
        CODE_INTERPRETER("code_interpreter"),
        RETRIEVAL("retrieval"),
        FUNCTION("function"),
        ;
        private final String name;
    }
}
