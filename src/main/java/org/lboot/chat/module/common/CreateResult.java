package org.lboot.chat.module.common;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.Map;

@Data
@ApiModel(value = "创建结果")
public class CreateResult {
    @ApiModelProperty(value = "ID")
    private String id;
    @ApiModelProperty(value = "类型")
    private String object;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss", timezone = "GMT+8")
    @JsonProperty("created_at")
    private Date createdAt;

    @ApiModelProperty(value = "元数据")
    private Map<String,Object> metadata;


}
