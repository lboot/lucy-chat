package org.lboot.chat.module.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "删除结果")
public class DeleteResult {
    @ApiModelProperty(value = "删除ID")
    private String id;
    @ApiModelProperty(value = "删除类型")
    private String object;
    @ApiModelProperty(value = "删除结果")
    private Boolean deleted;
}
