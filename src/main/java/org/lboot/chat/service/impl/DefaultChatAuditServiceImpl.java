package org.lboot.chat.service.impl;

import org.lboot.chat.service.ChatAuditService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DefaultChatAuditServiceImpl implements ChatAuditService {
    @Override
    public String getUserId() {
        return ChatAuditService.super.getUserId();
    }

    @Override
    public String getUserName() {
        return ChatAuditService.super.getUserName();
    }

}
