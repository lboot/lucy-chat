package org.lboot.chat.service;

/**
 * @author kindear
 * chat 审计服务
 */
public interface ChatAuditService {
    // 获取当前操作用户ID
    default String getUserId(){
        return "0";
    }

    default String getUserName(){
        return "匿名用户";
    }

}
