package org.lboot.chat.service;

import org.lboot.chat.module.common.message.ChatMessage;

import java.util.List;

/**
 * @author kindear
 * 定义缓存服务
 */
public interface ChatCacheService {
    /**
     * 根据会话记录查询历史记录
     * @param chatId
     * @return
     */
    List<ChatMessage> history(String chatId);

    /**
     * 查询历史记录
     * @param chatId 会话ID
     * @param limit 限制条数
     * @return
     */
    List<ChatMessage> history(String chatId,Integer limit);

    /**
     * 消息写入
     * @param message
     */
    void write(String chatId, ChatMessage message);

}
