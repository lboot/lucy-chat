package org.lboot.chat.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.lboot.chat.api.file.FileApi;
import org.lboot.chat.module.common.DeleteResult;
import org.lboot.chat.module.file.model.ChatFile;
import org.lboot.chat.module.file.params.WebFileParams;
import org.lboot.core.domain.ResponseDTO;
import org.springframework.web.bind.annotation.*;

/**
 * @author kindear
 * 文件管理
 */
@Slf4j
@RestController
@RequestMapping("openai")
@Api(tags = "OpenAI文件管理")
@AllArgsConstructor
public class ChatFileController {

    FileApi fileApi;

    @GetMapping("files")
    @ApiOperation(value = "文件列表")
    public Object listFiles(){
        return fileApi.listFiles();
    }


    @PostMapping("files")
    @ApiOperation(value = "上传文件")
    @SneakyThrows
    public ResponseDTO<ChatFile> uploadFile(WebFileParams params){
        return ResponseDTO.succData(fileApi.uploadFile(params));
    }

    @DeleteMapping("files/{file_id}")
    @ApiOperation(value = "删除文件")
    @SneakyThrows
    public ResponseDTO<DeleteResult> deleteFile(@PathVariable("file_id") String fileId){
        return ResponseDTO.succData(fileApi.deleteFile(fileId));
    }

    // @TODO 是下载接口
    @GetMapping("files/{file_id}/content")
    @ApiOperation(value = "文件内容")
    public ResponseDTO<Object> getFileContent(@PathVariable("file_id") String fileId){
        return ResponseDTO.succData(fileApi.getFileContent(fileId));
    }

}
