package org.lboot.chat.controller;

import org.lboot.chat.anno.ChatAuth;
import org.lboot.chat.dto.ChatDTO;
import org.lboot.chat.dto.EditDTO;
import org.lboot.chat.dto.ImageDTO;
import org.lboot.chat.module.chat.ChatParams;
import org.lboot.chat.module.chat.ChatResult;
import org.lboot.chat.module.common.message.ChatMessage;
import org.lboot.chat.module.edit.EditParams;
import org.lboot.chat.module.edit.EditResult;
import org.lboot.chat.module.edit.constant.EditModelEnum;
import org.lboot.chat.module.image.ImageParams;
import org.lboot.chat.module.image.ImageResult;
import org.lboot.chat.service.ChatCacheService;
import org.lboot.chat.module.chat.service.ChatService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author kindear
 * chat 聊天相关接口
 */
@Slf4j
@RestController
@RequestMapping("openai")
@Api(tags = "OpenAI服务")
public class ChatController {
    @Autowired
    ChatService chatService;

    @Autowired
    ChatCacheService cacheService;


    @ChatAuth
    @PostMapping("chat")
    @ApiOperation(value = "聊天",notes = "")
    public Object doChat(@RequestBody ChatDTO dto){
        ChatParams params = new ChatParams();

        List<ChatMessage> messages = cacheService.history(dto.getChatId(), dto.getWithContext());
        // 构建用户信息
        ChatMessage userMsg = chatService.buildUserMessage(dto.getContent());
        messages.add(userMsg);
        params.setMessages(messages);
        // 写入缓存服务
        cacheService.write(dto.getChatId(), userMsg);
        // 执行
        ChatResult result = chatService.doChat(params, dto.getChatId());

        return chatService.simpleResult(result);
    }



    @ChatAuth
    @PostMapping("edit/text")
    @ApiOperation(value = "文本编辑",notes = "")
    public Object doEditText(@Validated @RequestBody EditDTO dto){
        EditParams params = new EditParams();
        params.setInput(dto.getContent());
        params.setInstruction(dto.getTips());
        params.setModel(EditModelEnum.TEXT.getModel());
        EditResult result = chatService.doEdit(params);
        return chatService.simpleResult(result);
    }

    @ChatAuth
    @PostMapping("edit/code")
    @ApiOperation(value = "代码编辑",notes = "")
    public Object doEditCode(@Validated @RequestBody EditDTO dto){
        EditParams params = new EditParams();
        params.setInput(dto.getContent());
        params.setInstruction(dto.getTips());
        params.setModel(EditModelEnum.CODE.getModel());
        EditResult result = chatService.doEdit(params);
        return chatService.simpleResult(result);
    }

    @ChatAuth
    @PostMapping("draw")
    @ApiOperation(value = "图像绘制",notes = "")
    public Object doDraw(@Validated @RequestBody ImageDTO dto){
        ImageParams params = new ImageParams();
        // 设置描述
        params.setPrompt(dto.getTips());
        // 设置返回结果个数
        params.setN(dto.getN());
        ImageResult result = chatService.doDraw(params);
        return chatService.simpleResult(result);
    }

    @ChatAuth
    @GetMapping("chat/{chatId}")
    @ApiOperation(value = "聊天记录",notes = "")
    public Object chatHistory(@PathVariable("chatId") String chatId,@RequestParam(value = "limit",defaultValue = "10") Integer limit){
        return cacheService.history(chatId,limit);
    }

}
