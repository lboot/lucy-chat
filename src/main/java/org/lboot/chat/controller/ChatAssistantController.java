package org.lboot.chat.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.lboot.chat.api.assistant.AssistantApi;
import org.lboot.chat.api.assistant.AssistantFileApi;
import org.lboot.chat.module.assistant.Assistant;
import org.lboot.chat.module.assistant.params.AssistantParams;
import org.lboot.chat.module.assistant.params.AssistantQueryParams;
import org.lboot.chat.module.common.ListResult;
import org.lboot.core.domain.ResponseDTO;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("openai")
@Api(tags = "OpenAI助理服务")
@AllArgsConstructor
public class ChatAssistantController {

    AssistantApi assistantApi;

    AssistantFileApi assistantFileApi;

    @PostMapping("assistants")
    @ApiOperation(value = "新建助理")
    public Assistant createAssistant(@RequestBody AssistantParams params){
        return assistantApi.createAssistant(params);
    }

    @DeleteMapping("assistants/{assistant_id}")
    @ApiOperation(value = "删除助理")
    public ResponseDTO<Object> deleteAssistant(@PathVariable("assistant_id") String assistantId){
        return ResponseDTO.succData(
                assistantApi.deleteAssistant(assistantId)
        );
    }

    @GetMapping("assistants")
    @ApiOperation(value = "助理列表")
    public ListResult<Assistant> listAssistant(AssistantQueryParams params){
        return assistantApi.listAssistant(params);
    }
    @PostMapping("assistants/{assistant_id}/files")
    @ApiOperation(value = "助理新增文件")
    public ResponseDTO<Object> attachAssistantFile(@PathVariable("assistant_id") String assistantId, @RequestParam("file_id") String fileId){
        return ResponseDTO.succData(assistantFileApi.attachAssistantFile(assistantId, fileId));
    }

}
