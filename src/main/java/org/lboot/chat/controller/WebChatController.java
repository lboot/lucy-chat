package org.lboot.chat.controller;

import cn.hutool.core.util.IdUtil;
import org.lboot.chat.module.web.AssistantParams;
import org.lboot.chat.module.web.WebChatParams;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;

@Slf4j
@Controller
@RequestMapping("chat")
@Api(tags = "网页聊天")
public class WebChatController {

    @SneakyThrows
    @ApiIgnore
    @GetMapping("{chatId}")
    @ApiOperation(value = "在线聊天室",notes = "")
    public ModelAndView webChatRoom(@PathVariable("chatId") String chatId, HttpServletResponse response){
        ModelAndView modelAndView = new ModelAndView();
        // 构建助理详细信息
        AssistantParams params = new AssistantParams();
        params.setName("GPT-3.5 聊天小助手");
        params.setAvatar("https://s1.ax1x.com/2023/03/20/ppNV4Fs.png");
        params.setIntro("随时为您服务");
        // 构建聊天初始化信息
        WebChatParams initParams = new WebChatParams();
        initParams.setChatId(chatId);
        initParams.setInitContent("小助手随时为您服务");
        modelAndView.setViewName("chat");
        modelAndView.addObject("assistant",params);
        modelAndView.addObject("init",initParams);
        return modelAndView;
    }

    @SneakyThrows
    @ApiIgnore
    @GetMapping("web")
    @ApiOperation(value = "在线聊天室",notes = "")
    public ModelAndView webChatRoom(){
        ModelAndView modelAndView = new ModelAndView();
        // 构建助理详细信息
        AssistantParams params = new AssistantParams();
        params.setName("GPT-3.5 聊天小助手");
        params.setAvatar("https://s1.ax1x.com/2023/03/20/ppNV4Fs.png");
        params.setIntro("随时为您服务");
        // 构建聊天初始化信息
        WebChatParams initParams = new WebChatParams();
        initParams.setChatId(IdUtil.fastSimpleUUID());
        initParams.setInitContent("小助手随时为您服务");
        modelAndView.setViewName("chat");
        modelAndView.addObject("assistant",params);
        modelAndView.addObject("init",initParams);
        return modelAndView;
    }

    @ApiIgnore
    @GetMapping("demo1")
    @ApiOperation(value = "英语聊天室",notes = "")
    public ModelAndView webChatDemo1(){
        ModelAndView modelAndView = new ModelAndView();
        // 构建助理详细信息
        AssistantParams params = new AssistantParams();
        params.setName("英语学习小助手");
        params.setAvatar("https://s1.ax1x.com/2023/03/20/ppNV4Fs.png");
        params.setIntro("随时为您服务");
        // 构建聊天初始化信息
        WebChatParams initParams = new WebChatParams();
        initParams.setChatId(IdUtil.fastSimpleUUID());
        initParams.setInitContent("有什么英语学习问题可以询问我哦");
        modelAndView.setViewName("chat");
        modelAndView.addObject("assistant",params);
        modelAndView.addObject("init",initParams);
        return modelAndView;
    }


    @ApiIgnore
    @GetMapping("demo2")
    @ApiOperation(value = "咨询小助手",notes = "")
    public ModelAndView webChatDemo2(){
        ModelAndView modelAndView = new ModelAndView();
        // 构建助理详细信息
        AssistantParams params = new AssistantParams();
        params.setName("咨询小助手");
        params.setAvatar("https://s1.ax1x.com/2023/03/20/ppNV4Fs.png");
        params.setIntro("随时为您服务");
        // 构建聊天初始化信息
        WebChatParams initParams = new WebChatParams();
        initParams.setChatId(IdUtil.fastSimpleUUID());
        initParams.setInitContent("有什么问题都可以问我");
        modelAndView.setViewName("chat");
        modelAndView.addObject("assistant",params);
        modelAndView.addObject("init",initParams);
        return modelAndView;
    }


}
