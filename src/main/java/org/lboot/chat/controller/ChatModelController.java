package org.lboot.chat.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.lboot.chat.api.model.ModelApi;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("openai")
@Api(tags = "OpenAI模型")
@AllArgsConstructor
public class ChatModelController {
    ModelApi modelApi;
    @GetMapping("models")
    @ApiOperation(value = "模型列表")
    public Object listModel(){
        return modelApi.listModels();
    }
}
