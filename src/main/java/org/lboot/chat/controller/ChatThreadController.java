package org.lboot.chat.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.lboot.chat.api.thread.MessageApi;
import org.lboot.chat.api.thread.RunApi;
import org.lboot.chat.module.common.CreateResult;
import org.lboot.chat.module.common.ListResult;
import org.lboot.chat.module.thread.message.ChatThreadMessage;
import org.lboot.chat.module.thread.params.ChatThreadMessageParams;
import org.lboot.chat.module.thread.params.ChatThreadParams;
import org.lboot.chat.module.thread.params.ChatThreadRunParams;
import org.lboot.chat.module.thread.run.ChatThreadRun;
import org.lboot.chat.module.thread.service.ChatThreadService;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("openai")
@Api(tags = "OpenAI会话")
@AllArgsConstructor
public class ChatThreadController {
    ChatThreadService threadService;

    MessageApi messageApi;

    RunApi runApi;

    @PostMapping("threads")
    @ApiOperation(value = "新建会话")
    public CreateResult createThreads(@RequestBody ChatThreadParams params){
        return threadService.createThread(params);
    }

//thread_7uUCubhROQUDUnGzwn3xPEko
    @PostMapping("threads/{thread_id}/messages")
    @ApiOperation(value = "新建会话消息")
    public ChatThreadMessage createThreadMessage(@PathVariable("thread_id") String threadId, @RequestBody ChatThreadMessageParams params){
        return messageApi.createThreadMessage(threadId, params);
    }

    @PostMapping("threads/{thread_id}/runs")
    @ApiOperation(value = "执行会话")
    public ChatThreadRun threadRuns(@PathVariable("thread_id") String threadId, @RequestBody ChatThreadRunParams params){

        return runApi.createThreadRun(threadId, params);
    }


    @GetMapping("threads/{thread_id}/messages")
    @ApiOperation(value = "消息列表")
    public ListResult<ChatThreadMessage> listThreadMessages(@PathVariable("thread_id") String threadId){
        return threadService.getThreadMessages(threadId);
    }
}
