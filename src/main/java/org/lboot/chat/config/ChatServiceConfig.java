package org.lboot.chat.config;

import org.lboot.chat.service.ChatAuditService;
import org.lboot.chat.service.ChatCacheService;
import org.lboot.chat.service.impl.DefaultChatAuditServiceImpl;

import org.lboot.chat.service.impl.DefaultChatCacheServiceImpl;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;

/**
 * @author kindear
 * chat服务配置信息
 */
@SpringBootConfiguration
public class ChatServiceConfig {
    // Chat 鉴权服务
    @Bean
    @ConditionalOnMissingBean(value = ChatAuditService.class)
    public ChatAuditService chatAuthService(){
        return new DefaultChatAuditServiceImpl();
    }

    // Chat 缓存服务
    @Bean
    @ConditionalOnMissingBean(value = ChatCacheService.class)
    public ChatCacheService chatCacheService(){
        return new DefaultChatCacheServiceImpl();
    }


}
