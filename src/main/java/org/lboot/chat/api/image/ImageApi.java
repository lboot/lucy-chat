package org.lboot.chat.api.image;

import org.lboot.chat.module.image.ImageParams;
import org.lboot.chat.module.image.ImageResult;
import org.lboot.mrest.annotation.Body;
import org.lboot.mrest.annotation.Headers;
import org.lboot.mrest.annotation.MicroRest;
import org.lboot.mrest.annotation.Post;

import java.util.Map;

@MicroRest
public interface ImageApi {
    // 图像生成接口
    @Post("#{openai.chat.host}/v1/images/generations")
    ImageResult ChatDraw(@Headers Map<String, Object> headers, @Body ImageParams params);

    //ForestResponse<ImageResult> ChatImageEdit(@Header Map<String, Object> headers, @JSONBody ImageParams params);
}
