package org.lboot.chat.api.edit;

import org.lboot.chat.module.edit.EditParams;
import org.lboot.chat.module.edit.EditResult;
import org.lboot.mrest.annotation.Body;
import org.lboot.mrest.annotation.Headers;
import org.lboot.mrest.annotation.MicroRest;
import org.lboot.mrest.annotation.Post;

import java.util.Map;

/**
 * @author kindear
 * 该接口即将废弃
 */
@MicroRest
public interface EditApi {
    // 文本或者代码编辑接口
    @Post("#{openai.chat.host}/v1/edits")
    EditResult ChatEdits(@Headers Map<String, Object> headers, @Body EditParams params);
}
