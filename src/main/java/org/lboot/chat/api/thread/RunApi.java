package org.lboot.chat.api.thread;

import org.lboot.chat.module.thread.params.ChatThreadRunParams;
import org.lboot.chat.module.thread.run.ChatThreadRun;
import org.lboot.mrest.annotation.Body;
import org.lboot.mrest.annotation.MicroRest;
import org.lboot.mrest.annotation.PathVar;
import org.lboot.mrest.annotation.Post;

@MicroRest
public interface RunApi {
    @Post(value = "#{openai.chat.host}/v1/threads/{thread_id}/runs",headers = {
            "Authorization: Bearer #{openai.chat.key}",
            "OpenAI-Beta: assistants=v1"
    })
    ChatThreadRun createThreadRun(@PathVar("thread_id") String threadId, @Body ChatThreadRunParams params);
}
