package org.lboot.chat.api.thread;

import org.lboot.chat.module.common.ListResult;
import org.lboot.chat.module.thread.message.ChatThreadMessage;
import org.lboot.chat.module.thread.params.ChatThreadMessageParams;
import org.lboot.mrest.annotation.*;

@MicroRest
public interface MessageApi {
    @Post(value = "#{openai.chat.host}/v1/threads/{thread_id}/messages",headers = {
            "Authorization: Bearer #{openai.chat.key}",
            "OpenAI-Beta: assistants=v1"
    })
    ChatThreadMessage createThreadMessage(@PathVar("thread_id") String threadId, @Body ChatThreadMessageParams message);

    @Get(value = "#{openai.chat.host}/v1/threads/{thread_id}/messages",headers = {
            "Authorization: Bearer #{openai.chat.key}",
            "OpenAI-Beta: assistants=v1"
    })
    ListResult<ChatThreadMessage> listThreadMessage(@PathVar("thread_id") String threadId);
}
