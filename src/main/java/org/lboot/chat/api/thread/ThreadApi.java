package org.lboot.chat.api.thread;

import org.lboot.chat.module.assistant.Assistant;
import org.lboot.chat.module.assistant.params.AssistantParams;
import org.lboot.chat.module.common.CreateResult;
import org.lboot.chat.module.common.DeleteResult;
import org.lboot.chat.module.thread.ChatThread;
import org.lboot.chat.module.thread.params.ChatThreadParams;
import org.lboot.mrest.annotation.*;

import java.util.Map;

@MicroRest
public interface ThreadApi {
    @Post(value = "#{openai.chat.host}/v1/threads",headers = {
            "Authorization: Bearer #{openai.chat.key}",
            "OpenAI-Beta: assistants=v1"
    })
    CreateResult createThread(@Body ChatThreadParams params);

    @Delete(value = "#{openai.chat.host}/v1/threads/{thread_id}",headers = {
            "Authorization: Bearer #{openai.chat.key}",
            "OpenAI-Beta: assistants=v1"
    })
    DeleteResult deleteThread(@PathVar("thread_id") String id);

    @Put(value = "#{openai.chat.host}/v1/threads/{thread_id}",headers = {
            "Authorization: Bearer #{openai.chat.key}",
            "OpenAI-Beta: assistants=v1"
    })
    CreateResult updateThread(@PathVar("thread_id") String id, @Body("metadata") Map<String,Object> map);

    @Get(value = "#{openai.chat.host}/v1/threads/{thread_id}",headers = {
            "Authorization: Bearer #{openai.chat.key}",
            "OpenAI-Beta: assistants=v1"
    })
    CreateResult getThread(@PathVar("thread_id") String id);



}
