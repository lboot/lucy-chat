package org.lboot.chat.api.assistant;

import org.lboot.chat.module.assistant.Assistant;
import org.lboot.chat.module.assistant.params.AssistantParams;
import org.lboot.chat.module.assistant.params.AssistantQueryParams;
import org.lboot.chat.module.common.DeleteResult;
import org.lboot.chat.module.common.ListResult;
import org.lboot.mrest.annotation.*;

@MicroRest
public interface AssistantApi {
    @Post(value = "#{openai.chat.host}/v1/assistants",headers = {
            "Authorization: Bearer #{openai.chat.key}",
            "OpenAI-Beta: assistants=v1"
    })
    Assistant createAssistant(@Body AssistantParams params);

    @Delete(value = "#{openai.chat.host}/v1/assistants/{assistant_id}",headers = {
            "Authorization: Bearer #{openai.chat.key}",
            "OpenAI-Beta: assistants=v1"
    })
    DeleteResult deleteAssistant(@PathVar("assistant_id") String assistantId);

    @Put(value = "#{openai.chat.host}/v1/assistants/{assistant_id}",headers = {
            "Authorization: Bearer #{openai.chat.key}",
            "OpenAI-Beta: assistants=v1"
    })
    Assistant updateAssistant(@PathVar("assistant_id") String assistantId, @Body AssistantParams params);

    @Get(value = "#{openai.chat.host}/v1/assistants",headers = {
            "Authorization: Bearer #{openai.chat.key}",
            "OpenAI-Beta: assistants=v1"
    })
    ListResult<Assistant> listAssistant(@Query AssistantQueryParams params);

}
