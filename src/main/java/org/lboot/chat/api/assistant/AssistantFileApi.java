package org.lboot.chat.api.assistant;

import org.lboot.chat.module.common.DeleteResult;
import org.lboot.chat.module.file.model.ChatFile;
import org.lboot.mrest.annotation.*;

/**
 * @author kindear
 * 助手文件接口
 */
@MicroRest
public interface AssistantFileApi {
    /**
     * 附加文件
     * @param assistantId
     * @param fileId
     * @return
     */
    @Post(value = "#{openai.chat.host}/v1/assistants/{assistant_id}/files",headers = {
            "Authorization: Bearer #{openai.chat.key}",
            "OpenAI-Beta: assistants=v1"
    })
    ChatFile attachAssistantFile(@PathVar("assistant_id") String assistantId, @Body("file_id") String fileId);
    /**
     * 删除文件
     * @param assistantId
     * @param fileId
     * @return
     */
    @Delete(value = "#{openai.chat.host}/v1/assistants/{assistant_id}/files/{file_id}",headers = {
            "Authorization: Bearer #{openai.chat.key}",
            "OpenAI-Beta: assistants=v1"
    })
    DeleteResult deleteAssistantFile(@PathVar("assistant_id") String assistantId, @PathVar("file_id") String fileId);
}
