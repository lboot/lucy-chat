package org.lboot.chat.api.chat;
import org.lboot.chat.module.chat.ChatParams;
import org.lboot.chat.module.chat.ChatResult;
import org.lboot.mrest.annotation.Body;
import org.lboot.mrest.annotation.MicroRest;
import org.lboot.mrest.annotation.Post;

/**
 * @author kindear
 * 聊天相关API
 */
@MicroRest
public interface ChatApi {
    // 新建聊天接口
    @Post(
            value = "#{openai.chat.host}/v1/chat/completions",
            headers = {
            "Authorization: Bearer #{openai.chat.key}"
            },
            readTimeout = 100
    )
    ChatResult ChatCompletions(@Body ChatParams params);

}
