package org.lboot.chat.api.file;

import org.lboot.chat.module.common.DeleteResult;
import org.lboot.chat.module.file.FileListResult;
import org.lboot.chat.module.file.model.ChatFile;
import org.lboot.chat.module.file.params.LocalFileParams;
import org.lboot.chat.module.file.params.WebFileParams;
import org.lboot.mrest.annotation.*;

@MicroRest
public interface FileApi {

    @Post(value = "#{openai.chat.host}/v1/files",headers = {
            "Content-Type:multipart/form-data",
            "Authorization: Bearer #{openai.chat.key}"
    })
    ChatFile uploadFile(@Body LocalFileParams params);

    @Post(value = "#{openai.chat.host}/v1/files",headers = {
            "Content-Type:multipart/form-data",
            "Authorization: Bearer #{openai.chat.key}"
    })
    ChatFile uploadFile(@Body WebFileParams params);


    @Delete(value = "#{openai.chat.host}/v1/files/{file_id}",headers = {
            "Authorization: Bearer #{openai.chat.key}"
    })
    DeleteResult deleteFile(@PathVar("file_id") String fileId);


    /**
     * 获取文件内容
     * @param fileId
     * @return
     */
    @Get(value = "#{openai.chat.host}/v1/files/{file_id}/content",headers = {
            "Authorization: Bearer #{openai.chat.key}"
    })
    String getFileContent(@PathVar("file_id") String fileId);

    @Get(value = "#{openai.chat.host}/v1/files",headers = {
            "Authorization: Bearer #{openai.chat.key}"
    })
    FileListResult listFiles();

}
