package org.lboot.chat.api.model;

import org.lboot.chat.module.model.ModelListResult;
import org.lboot.mrest.annotation.Get;
import org.lboot.mrest.annotation.MicroRest;

/**
 * @author kindear
 * 模型相关接口
 */
@MicroRest
public interface ModelApi {
    @Get(value = "#{openai.chat.host}/v1/models",headers = {
            "Authorization: Bearer #{openai.chat.key}"
    })
    ModelListResult listModels();
}
