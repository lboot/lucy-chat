package org.lboot.chat.cache;

import cn.hutool.cache.Cache;
import cn.hutool.cache.CacheUtil;
import cn.hutool.core.lang.Validator;
import lombok.extern.slf4j.Slf4j;
import org.lboot.chat.module.common.message.ChatMessage;

import java.util.ArrayList;
import java.util.List;

/**
 * 携带信息
 */
@Slf4j
public class ChatCache {
    // 本地缓存的信息 -- 修改 capacity 设置最大容量
    public static Cache<String, List<ChatMessage>> chats = CacheUtil.newLFUCache(100);

    public static void put(String key, List<ChatMessage> value){
        chats.put(key,value);
    }


    public static List<ChatMessage> get(String key){
        List<ChatMessage> messages = chats.get(key);

        if (Validator.isEmpty(messages)){
            return new ArrayList<>();
        }else {
            return messages;
        }
    }
}
