package org.lboot.chat.anno;

import java.lang.annotation.*;

/**
 * @author kindear
 * lucy-chat 鉴权拦截
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD,ElementType.TYPE})
public @interface ChatAuth {
    // it is anything
    String value() default "";
}
