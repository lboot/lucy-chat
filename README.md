<p align="center">
	<a href="https://lucy.apisev.cn/"><img src="https://s1.ax1x.com/2023/04/10/ppqZQjf.png" width="15%"></a>
</p>
<p align="center">
	<strong>Lucy v1.0.0</strong>
</p>
<p align="center">
	<strong>不断迭代的技术解决方案</strong>
</p>

---

<p align="center">
	<strong>lucy-chat</strong>
</p>



![](https://img.shields.io/badge/build-passing-green)	![](https://img.shields.io/badge/license-MIT-blue)

## 简介

`lucy-chat`是基于`springboot`框架接入`ChatGPT`的轻量级解决方案，目前已实现功能如下：

- [x] 聊天接口接入
- [x] 聊天记录
- [x] 聊天上下文支持
- [x] 编辑接口接入
- [x] 绘图接口接入
- [x] 内部集成聊天`html`
- [x] `Latex`数学公式解析
- [x] 代码段格式解析
- [x] 自定义鉴权拓展
- [x] 持久化存储拓展
- [x] 支持`pom`引入
- [ ] ...

## 安装

`lucy-chat`提供了两种形式进行服务接入



### 1. Jar引入

> 准备

在引入任何 `Lucy`系列依赖之前，需要完成`jitpack`镜像仓库的配置。

```xml
<repositories>
        <repository>
            <id>jitpack.io</id>
            <url>https://www.jitpack.io</url>
        </repository>
</repositories>
```

> 引入	

根据版本号引入 (与发行版对应 例 1.0.0)

```xml
		<dependency>
			<groupId>com.gitee.lboot</groupId>
			<artifactId>lucy-chat</artifactId>
			<version>${version}</version>
		</dependency>
```

> 启动类

启用 `knife4j` 文档，需要在启动类上配置 `@EnableKnife4j`

```java
@EnableKnife4j
@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {

        SpringApplication.run(LucyAdminApplication.class, args);
    }

}

```

> 配置文件

使用 `lucy-chat`需要配置如下文件信息

```properties
spring.application.name=lucy-chat
# 运行端口
server.port=8080
# swagger 匹配
spring.mvc.pathmatch.matching-strategy=ant_path_matcher

# chat-gpt api-key
# 申请地址 https://platform.openai.com/account/api-keys
openai.chat.key=

# chat-gpt proxy host
# 配置代理地址 请参阅 https://www.v2ex.com/t/921689
openai.chat.host=

# 连接池最大连接数
forest.max-connections=1000
# 连接超时时间，单位为毫秒
forest.connect-timeout=30000
# 数据读取超时时间，单位为毫秒
forest.read-timeout=30000


```

### 2. 独立服务

1. 从开源地址下载项目

```shell
git clone https://gitee.com/lboot/lucy-chat.git
```

2. 修改`POM`文件中打包方式，即恢复 `<build>`相关注释掉的内容

```xml
<build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.1</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                    <encoding>UTF-8</encoding>
                </configuration>

            </plugin>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <version>${spring-boot.version}</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>repackage</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
```

3. 修改相关配置文件, 参考上文的配置文件相关内容，项目中提供的 `key`为私人 `key`，随时会被替换。
4. 打包项目并部署



## 使用

完成后可以参阅表格访问`[服务地址]/路径`

| 功能         | 路径             |
| ------------ | ---------------- |
| `API文档`    | `/doc.html`      |
| 聊天窗口     | `/chat/web`      |
| 指定聊天窗口 | `/chat/{chatId}` |

![](static/img_1.png)

## 文档

更多功能介绍和功能拓展请参阅

[更多文档](https://lucy.apisev.cn/#/lucy-chat/)


## 技术支持

<kindear@foxmail.com>